//
//  ButtonExtension.swift
//  UrbanSights
//
//  Created by Martyna Wiśnik on 21/09/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    
    func buttonSettings() {
        
        self.layer.cornerRadius = self.frame.height/2
        self.clipsToBounds = true
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.masksToBounds = true
       
}

}
