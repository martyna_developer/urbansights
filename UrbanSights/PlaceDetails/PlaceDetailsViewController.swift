//
//  PlaceDetailsViewController.swift
//  UrbanSights
//
//  Created by Martyna Wiśnik on 07/09/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class PlaceDetailsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var fullPlaceImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var alertLabel: UILabel!
    @IBAction func showAlertView(_ sender: Any) {
    showAlert()
    }
    @IBOutlet weak var mapView: MKMapView!
    @IBAction func openWebsite(_ sender: Any) {
        if let requestUrl = NSURL(string: place.url ?? "") {
            UIApplication.shared.openURL(requestUrl as URL)
        }
    }
    
    var place = PlaceModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        fullPlaceImage.image = place.image
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 50
        infoButton.buttonSettings()
        alertView.isHidden = true
        infoButton.isUserInteractionEnabled = true
        alertLabel.text = place.title
        centerMap()
        alertView.layer.cornerRadius = 25
        alertView.layer.masksToBounds = true
    }
    
    func centerMap() {
        let center = CLLocationCoordinate2D(latitude: place.pin?.coordinate.latitude ?? 0.0, longitude: place.pin?.coordinate.longitude ?? 0.0)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.0002, longitudeDelta: 0.0002))
        
        self.mapView.setRegion(region, animated: true)
    }
    
    func showAlert() {
        alertView.isHidden = !alertView.isHidden
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
            
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: "placeDescriptionTextCell"), for: indexPath) as! PlaceDescriptionTextCell
            
            cell.descriptionLabel.text = place.description
            
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: "addressCell"), for: indexPath) as! AddressCell
            
            cell.addressLabel.text = place.address
            
            return cell
  
        default:
            return UITableViewCell()
        }
    }
    
}
