//
//  PlaceDescriptionTextCell.swift
//  UrbanSights
//
//  Created by Martyna Wiśnik on 08/09/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

class PlaceDescriptionTextCell: UITableViewCell {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
}
