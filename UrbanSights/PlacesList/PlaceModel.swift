//
//  PlaceModel.swift
//  UrbanSights
//
//  Created by Martyna Wiśnik on 07/09/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class PlaceModel {
    
    var id: String?
    var image: UIImage?
    var title: String?
    var description: String?
    var pin: PinModel?
    var address: String?
    var url: String?
    
    init(id: String? = nil, image: UIImage? = nil, title: String? = nil, description: String? = nil, pin: PinModel? = nil, address: String? = nil, url: String? = nil) {
        self.id = id
        self.image = image
        self.title = title
        self.description = description
        self.pin = pin
        self.address = address
        self.url = url
    }
    
}
