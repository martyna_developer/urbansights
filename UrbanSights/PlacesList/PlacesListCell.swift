//
//  PlacesListCell.swift
//  UrbanSights
//
//  Created by Martyna Wiśnik on 07/09/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

class PlacesListCell: UITableViewCell {
    
    @IBOutlet weak var placeImageView: UIImageView!
    @IBOutlet weak var mapButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mapButton.buttonSettings()
    }
    
}
