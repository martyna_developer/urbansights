//
//  PlacesListViewController.swift
//  UrbanSights
//
//  Created by Martyna Wiśnik on 07/09/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class PlacesListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    lazy var placesList = PlacesListModel()
    lazy var chosenPlaceCoord = CLLocationCoordinate2D()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placesList.places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "placesListCell") as! PlacesListCell
        
        cell.placeImageView.image = placesList.places[indexPath.row].image
        cell.isUserInteractionEnabled = true
        cell.mapButton.addTarget(self, action: #selector(openMap(sender:)), for: .touchUpInside)
        cell.mapButton.tag = indexPath.row
    
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    @objc private func openMap(sender: UIButton) {
        self.tabBarController?.selectedIndex = 0
        if let navController = self.tabBarController?.selectedViewController as? UINavigationController? {
            if let mapController = navController?.viewControllers.first as? MapViewController {
            mapController.mapView.setCenter(placesList.places[sender.tag].pin?.coordinate ?? CLLocationCoordinate2D(), animated: true)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetails" {
            if let indexPath = tableView.indexPathForSelectedRow {
                if let destinationController = segue.destination as? PlaceDetailsViewController {
                    destinationController.place = placesList.places[indexPath.row]
                }
            }
        }
    }
    
    
}
