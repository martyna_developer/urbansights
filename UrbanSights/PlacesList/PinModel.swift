//
//  PinModel.swift
//  UrbanSights
//
//  Created by Martyna Wiśnik on 07/09/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit
import MapKit


class PinModel: NSObject, MKAnnotation {
    
    
    var id: String?
    var title: String?
    var coordinate: CLLocationCoordinate2D
    
    init(id: String?, title: String?, coordinate: CLLocationCoordinate2D) {
        
        self.id = id
        self.title = title
        self.coordinate = coordinate
        
        super.init()
    }
}
