//
//  ColorExtension.swift
//  UrbanSights
//
//  Created by Martyna Wiśnik on 08/09/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    class var lightYellow: UIColor {
        return UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 224.0/255.0, alpha: 1.0)
}
}

