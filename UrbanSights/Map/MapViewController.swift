//
//  MapViewController.swift
//  UrbanSights
//
//  Created by Martyna Wiśnik on 07/09/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class MapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBAction func centerButton(_ sender: UIButton) {
        centerMapOnUser()
    }
    
    var locationManager: CLLocationManager = CLLocationManager()
    let placesList = PlacesListModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        self.locationManager.startMonitoringVisits()
        locationManager.requestAlwaysAuthorization()
        locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
        AnnotationFactory.addAnnotations(mapView: self.mapView, placesList: placesList)
        mapView.userTrackingMode =  .follow
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBarItem.image = UIImage(named: "")
        self.tabBarItem.selectedImage = UIImage(named: "")
    }
    
    @objc func centerMapOnUser() {
        mapView.setUserTrackingMode(MKUserTrackingMode.follow, animated: true)
    }
    
    private func locationManager(manager: CLLocationManager,
                                 didFailWithError error: NSError) {
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let lastLocation = locations.last else { return }
        print(lastLocation.coordinate.longitude)
        print(lastLocation.coordinate.latitude)
    }
}


