//
//  AnnotationFactory.swift
//  UrbanSights
//
//  Created by Martyna Wiśnik on 07/09/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit
import MapKit

final class AnnotationFactory {
    
    static func addAnnotations(mapView: MKMapView, placesList: PlacesListModel) {
        mapView.clearsContextBeforeDrawing = true
        for place in placesList.places {
            if let pin = place.pin {
                mapView.addAnnotation(pin)
            }
        }
    }
}
